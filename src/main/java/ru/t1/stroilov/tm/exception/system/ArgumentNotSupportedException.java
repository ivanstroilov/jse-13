package ru.t1.stroilov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String message) {
        super("Error! Argument \"" + message + "\" not supported...");
    }
}
