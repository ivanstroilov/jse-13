package ru.t1.stroilov.tm.api.model;

import ru.t1.stroilov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
